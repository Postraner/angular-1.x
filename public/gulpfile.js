var gulp = require('gulp'),
    sass = require('gulp-sass'),
    concat = require('gulp-concat'),
    uglify = require('gulp-uglify'),
    sourcemaps = require('gulp-sourcemaps'),
    myth = require('gulp-myth'),
    csso = require('gulp-csso');


// PATH
var path = {
    build: "./build",
    src: { 
        js: './javascripts/**/**.js',
        style: './stylesheets/main.scss'
    },
    watch: {
        js: './javascripts/**/**.js',
        style: './stylesheets/**.scss'
    },
    clean: './build'
};

// --BUILD--

// SCSS
gulp.task('scss:build', function () {
  return gulp.src(path.src.style)
    .pipe(sourcemaps.init())
    .pipe(concat('style.scss'))
    .pipe(sourcemaps.write())
    .pipe(sass.sync().on('error', sass.logError))
    .pipe(gulp.dest(path.build));
});

// JS
gulp.task('js:build', function () {
  return gulp.src(path.src.js)
    .pipe(sourcemaps.init())
    .pipe(sourcemaps.write())
    .pipe(concat('app.js'))
    .pipe(gulp.dest(path.build))
});
gulp.task('libsJS:build', function() {
  return gulp.src([
      './bower_components/angular/angular.min.js',
      './bower_components/angular-ui-router/release/angular-ui-router.js',
      './bower_components/angular-resource/angular-resource.js',
      './bower_components/angular-permission/dist/angular-permission.js',
      './bower_components/angular-animate/angular-animate.js',
      './bower_components/angular-aria/angular-aria.js',
      './bower_components/angular-material/angular-material.js',
      './bower_components/angular-messages/angular-messages.js',
      './bower_components/moment/moment.js'
  ])
    .pipe(concat('vendor.js'))
    .pipe(uglify())
    .pipe(gulp.dest(path.build));
});
gulp.task('libsCSS:build', function() {
    return gulp.src(['./bower_components/angular-material/angular-material.scss'])
        .pipe(sass.sync().on('error', sass.logError))
        .pipe(concat('vendor.css'))
        .pipe(csso())
        .pipe(gulp.dest(path.build));
});
// --WATCH--
gulp.task('watch', function(){
     gulp.watch([path.watch.js], function(event, cb) {
        gulp.start('js:build');
    });
     gulp.watch([path.watch.style], function(event, cb) {
        gulp.start('scss:build');
    });
});

gulp.task('dev:min',[
    'libsJS:build',
    'libsCSS:build',
    'js:build',
    'scss:build',
    'watch'],
    function () {
    gulp.src('./build/**.js').pipe(uglify());
    gulp.src('./build/**.css').pipe(csso());
});

gulp.task('build', ['libsJS:build', 'libsCSS:build'], function () {
    gulp.src(path.src.style)
        .pipe(sourcemaps.init())
        .pipe(concat('style.scss'))
        .pipe(sass.sync().on('error', sass.logError))
        .pipe(myth())
        .pipe(csso())
        .pipe(sourcemaps.write())
        .pipe(gulp.dest(path.build));
    gulp.src(path.src.js)
        .pipe(sourcemaps.init())
        .pipe(sourcemaps.write())
        .pipe(concat('app.js'))
        .pipe(uglify())
        .pipe(gulp.dest(path.build))
});

gulp.task('default', [
    'libsJS:build',
    'libsCSS:build',
    'js:build',
    'scss:build',
    'watch']);

 
