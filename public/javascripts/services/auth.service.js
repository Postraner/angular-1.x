function authService($resource) {
    var url = 'http://localhost:3000/api/auth/';
    return $resource(url, {email: '@email', password: '@password'}, {
        signin: {
            url: url + 'signin',
            method: 'POST'
        },
        signup: {
            url: url + 'signup',
            method: 'POST'
        }
    })
}

angular.module('blog')
    .factory('AuthService', ['$resource', authService]);