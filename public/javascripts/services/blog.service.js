function blogService($resource) {
    var url = 'http://localhost:3000/api/blog/:postId';
    var token = sessionStorage.getItem('auth_token');
    return $resource(url, {}, {
        getPosts: {
            method: 'GET',
            isArray: true
        },
        add: {
            method: 'POST',
            headers: {
                token: token
            }
        },
        update: {
            method: 'PUT',
            headers: {
                token: token
            }
        },
        delete: {
            method: 'DELETE',
            headers: {
                token: token
            }
        }
    })
}


angular
    .module('blog')
        .factory('BlogService', ['$resource', blogService]);