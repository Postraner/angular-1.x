angular
    .module('blog', [
        'ui.router',
        'ngResource',
        'ngMessages',
        'permission',
        'ngMaterial'
        ])

    .config(function ($stateProvider, $urlRouterProvider, $locationProvider) {

        $locationProvider.html5Mode(true);

        $urlRouterProvider.otherwise('/');
        $stateProvider
            .state('blog', {
                url: '/',
                templateUrl: '/static/blog.html',
                controller: 'blogCtrl as blog'
            })
            .state('post', {
                url: '/post/{postId}',
                templateUrl: '/static/post.html',
                controller: 'postCtrl as post'
            })
            .state('auth', {
                url: '/auth',
                templateUrl: '/static/auth.html',
                controller: 'authCtrl as auth'
            });
    })

    .run(function (PermPermissionStore) {
        PermPermissionStore
            .definePermission('editBlog', function () {
                return true;
            });
    });