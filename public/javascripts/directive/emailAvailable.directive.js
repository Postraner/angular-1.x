function emailAvailable($http, $q) {
    return {
        require: 'ngModel',
        link: function (scope, element, attrs, ctrl) {
           return ctrl.$asyncValidators.emailAvailable = function (modelValue, viewValue) {
                var value = viewValue || modelValue;
                if(value && value.length >= 5) {
                    return $http.get("http://localhost:3000/api/auth/check/" + value)
                            .catch(function(error) {
                               if(error.status === 406) return $q.reject();
                            })
                } else {
                    return $q.resolve();
                }
            }
        }
    }
}

angular
    .module('blog')
        .directive('emailAvailable', ['$http', '$q', emailAvailable]);