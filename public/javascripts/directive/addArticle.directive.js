function addArticle() {
    var templateBnt =  "<md-button class=\"md-raised md-primary\" ng-click=\"popup.showPopup($event)\" permission permission-only=\"'AUTHORIZED'\">Add article </md-button>";
    return {
        restrict: 'E',
        template: templateBnt ,
        controller: 'articlePopupCtrl as popup'
    }
}
angular
    .module('blog')
        .directive('addArticle', [addArticle]);



