function blogCtrl(
    $location,
    $scope,
    $mdDialog,
    BlogService,
    PermRoleStore) {

    var vm = this;

    vm.sortReverse = true;
    vm.sortArticles = function () {
        vm.sortReverse = !vm.sortReverse
    };

    if (sessionStorage.getItem('auth_token')) {
        PermRoleStore
            .defineRole('AUTHORIZED', ['editBlog']);
    }

    vm.logout = function () {
        PermRoleStore.clearStore();
        sessionStorage.clear();
        $location.path('/');
    };
    function getAllPosts() {
        BlogService.getPosts().$promise.then(
            function (res) {
                vm.posts = [];
                res.map(function (item) {
                    item.edit = false;
                    item.created = moment(item.created).format('LLL');
                    vm.posts.push(item);
                })
            },
            function (err) {
                console.log(err)
            }
        );
    }
    getAllPosts();

    $scope.$on('update', function () {
        getAllPosts();
    });

    vm.editPost = function (id ) {
        var post = {
            title: vm.edit.title,
            text: vm.edit.text
        };
        BlogService.update({
            postId: id
        }, post).$promise.then(
            function () {
                getAllPosts();
            },
            function (err) {
                if (err.status === 403) vm.logout();
                console.log('ERROR!', err)
            }
        );
    };

    vm.removePost = function (id, post, ev) {
        var confirm = $mdDialog.confirm()
            .title('Are you sure you want to remove the article?')
            .textContent('If you delete this article, you can not restore it never')
            .targetEvent(ev)
            .ok('Yes')
            .cancel('Cancel');
        $mdDialog.show(confirm).then(function() {
            BlogService.delete({postId: id}).$promise
                .then(function () {
                    var postIndex = vm.posts.indexOf(post);
                    if (postIndex !== -1) vm.posts.splice(postIndex, 1)
                })
                .catch(function (err) {
                    if (err.status === 403) {
                        vm.logout();
                    }
                    console.log('ERROR!', err)
                });
        }, function() {
           console.log('Canceled');
        });

    };

}

angular
    .module('blog')
        .controller('blogCtrl', [
            '$location',
            '$scope',
            '$mdDialog',
            'BlogService',
            'PermRoleStore',
            blogCtrl
        ]);