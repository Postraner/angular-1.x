function postCtrl($location, $http,$mdDialog, $stateParams, BlogService) {

    var vm = this;

    var url = 'http://localhost:3000/api/blog/' + $stateParams.postId;

    $http.get(url)
        .then(
            function (res) {
               vm.data = res.data
            },
             function (err) {
                console.log(err)
             }
        );
    vm.remove = function (id, ev) {
        var confirm = $mdDialog.confirm()
            .title('Would you like to delete this article?')
            .textContent('If you delete this article, you can not restore it never')
            .targetEvent(ev)
            .ok('Remove')
            .cancel('Cancel');
        $mdDialog.show(confirm).then(function() {
            BlogService.delete({ postId: id}).$promise
                .then(function () {
                    $location.path('/');
                })
                .catch(function (err) {
                    if (err.status === 403) vm.logout();
                    console.log('ERROR!', err)
                });
        }, function() {
            console.log('Canceled');
        });
    }
}
angular
    .module('blog')
        .controller('postCtrl', [
            '$location',
            '$http',
            '$mdDialog',
            '$stateParams',
            'BlogService',
            postCtrl
        ]);
