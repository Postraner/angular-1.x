function authCtrl(
    AuthService,
    $location,
    $scope,
    $window,
    PermRoleStore) {

    var vm = this;
    
    vm.username = sessionStorage.getItem('username');

    if (sessionStorage.getItem('auth_token')) {
        PermRoleStore
            .defineRole('AUTHORIZED', ['editBlog']);
    }
    function signin(user) {
        AuthService.signin(user).$promise.then(
            function (res) {
                sessionStorage.setItem('username', user.email);
                sessionStorage.setItem('auth_token', res.token);
                $location.path('/');
                $window.location.reload();
            },
            function (err) {
                $scope.loginForm.$valid = false;
                console.log('ERROR!', err)
            }
        );
    }

    vm.logout = function () {
        PermRoleStore.clearStore();
        sessionStorage.clear();
        $window.location.reload();
    };
    vm.login = function () {
        signin({
            email: vm.email,
            password: vm.pass
        });
    };
    vm.signup = function () {
        var user = {
            email: vm.reg_email,
            password: vm.reg_pass
        };
        AuthService.signup(user).$promise
            .then(
                function (res) {
                    signin({
                        email: res.email,
                        password: res.password
                    });
                },
                function (err) {
                    if (err.status === 403) vm.logout();
                    $scope.signUpForm.reg_email.$error.invalidEmail = true
                    console.log('ERROR!', err)
                }
            )
    };
}

angular.module('blog')
    .controller('authCtrl', [
        'AuthService',
        '$location',
        "$scope",
        '$window',
        'PermRoleStore',
        authCtrl]);