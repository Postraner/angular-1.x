function articlePopup($mdDialog, $scope, BlogService) {
    var vm = this;

    vm.showPopup = function (ev) {
        $mdDialog.show({
            templateUrl: '/partials/popup.html',
            parent: angular.element(document.body),
            targetEvent: ev,
            clickOutsideToClose: true,
            controller: 'articlePopupCtrl as popup'
        }).then(function() {
            $scope.$emit('update');
        }, function() {
           console.log('Cancel');
        });
    };

    vm.cancel = function () {
        $mdDialog.cancel();
    };

    vm.addPost = function () {

        var post = {
            title: vm.article.title,
            text: vm.article.text
        };
        if(post.title || post.text )
        BlogService.add(post).$promise.then(
            function () {
                $mdDialog.hide();
            },
            function (err) {
                if (err.status === 403) {
                    vm.logout();
                }
                console.log('ERROR!', err)
            }
        );
    };
}

angular
    .module('blog')
        .controller('articlePopupCtrl', [
            '$mdDialog',
            '$scope',
            'BlogService',
            articlePopup
        ]);